#!/usr/bin/env python3

### Importing Necessary Modules ###
import csv
import math

import numpy as np
import matplotlib.pyplot as plt
#-- END of Importing --#

#Calculating Resistance at zero temperature
def calculate_R0():

    T_d = float(input("Enter Room Temperature (Degree Celcius): "))

    A = 4.82e-3 #Alpha
    B = 6.76e-7 #Beta

    #Reading data for calculating R0
    with open('A12_R0.csv', newline='') as file:
        cursor = csv.reader(file)
        SUM = 0     #Initializing SUM for averaging purpose
        COUNT = 0   #Initializing COUNT for averaging purpose
        for row in cursor:
            try:
                #          Current (mA)   Voltage (V)
                (r1,r2) = (float(row[0]), float(row[1]))
                COUNT = COUNT + 1
                rms = r2/(2e0*math.sqrt(2e0))   #Converting to rms Voltage

                #Calculating Resistance for each reading
                #Multiplied 1000 to convert mA -> A
                R = rms*1000/r1

                #Summing up resistances
                SUM = SUM + R

            except:
                continue

        AVERAGE = SUM / COUNT               #Calculaing resistance after going through the table
        T_k = T_d + 273e0                   #Converting Degree Celcius to Kelvin
        denum = 1e0 + A*T_k + B*T_k*T_k     #Calculating denominator of the formula given
        R0 = AVERAGE/denum                  #Resistance at 0 K has been calculated

        print("\nZero temperature resistance, R0 = {}\n".format(R0))
        return R0

#Calculating Temperature of the Fillament for any Resistance
def Temperature(R, R0):
    A = 4.82e-3 #Alpha
    B = 6.76e-7 #Beta

    #Inside Sqrt
    IS = A*A + 4e0*B*( R/R0 - 1e0 )

    c  = 1e0/(2e0*B)
    result = 273e0 + c*(math.sqrt(IS) - A)
    return result

#Calculating Resistance R using U1/I and then
#Calculating Temperature using Resistance R 
def calculate():
    R0  = calculate_R0()
    U2  = []
    T   = []
    R_t = []
    with open('A12_Data.csv', newline='') as file:
        print("I(A)\tU1(V)\tU2(mV)\tR=U1/I\tT(K)")
        cursor = csv.reader(file)
        for row in cursor:
            try:
                #             #Current(A)    #U1 (V)        #U2 (mV)
                (r1,r2,r3) = (float(row[0]), float(row[1]), float(row[2]))
                R = r2/r1
                print("{}\t{}\t{}\t{}\t{}".format(r1,r2,r3,round(R,2),round(Temperature(R,R0),2)))

                #Skipping zero or -ve values
                #since log(0) is invalid
                if r3 <= 0: continue

                T.append(Temperature(R,R0))
                U2.append(r3)
            except:
                continue

    #Converting List -> Array
    X = np.asarray(T)
    Y = np.asarray(U2)

    X_log = np.log10(X)
    Y_log = np.log10(Y)

    #Applying Linear fitting
    #Calculating Slope and Intercept
    #m -> Slope, b -> Intercept
    m,b = np.polyfit(X_log, Y_log,1)

    print("\nSlope after linear fitting: {}\n".format(m))

    #Writing the resulting data to a CSV file
    with open('results.csv', 'w', newline='') as file:
        cursor = csv.writer(file)
        cursor.writerow(['Temperature T (K)', 'U2 (mV)'])

        for i in range(len(T)):
            cursor.writerow([T[i] , U2[i]])
    print("###### Checkout results.csv ######")

    #Plotting --------------------------------------------
    z = np.polyfit(X_log,Y_log,1)
    f = np.poly1d(z)

    yfit = lambda x: np.power(10.00,f(np.log10(x)))


        #Adding text
    fig, ax = plt.subplots()
    textstr = 'Slope, m = {}'.format(m)

    # these are matplotlib.patch.Patch properties
    props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)

    # place a text box in upper left in axes coords
    ax.text(0.03, 0.85, textstr, transform=ax.transAxes, fontsize=12,
            verticalalignment='top', bbox=props)

    ax.loglog(X,Y,'ro-',X,yfit(X))
    plt.xlabel(r'Temperature, T (K) ---->')
    plt.ylabel(r'Thermopile Voltage, $U_2$ (mV) ---->')
    plt.title(r'Log-Log Thermopile Voltage $U_2$ Vs Temperature T Plot')
    ax.legend(['Experimental Values', 'Linear Fitting'])
        #End of Adding Text
    plt.show()
    #END of Plotting ---------------------------------------

#Solved some problems from https://stackoverflow.com/questions/18760903/fit-a-curve-using-matplotlib-on-loglog-scale  

#Main function
if __name__ == '__main__':
    calculate()
    input("\nPress ENTER to exit")
