# Preambles      
      
To run this projects. The requirements are:      
      
    1. Python
    2. Git
    3. Microsoft Excel
      
Required Python modules:  
  
    1. numpy    
    2. matplotlib    
  
  I already installed these on our Lab PC.  


If you want to try his project on your own pc, make sure "Add Python to path" is ticked  ✅   while installing Python

To install the Python modules, right-click windows button >> Windows powershell.  
Then ENTER this command:  
  
```
python -m pip install numpy matplotlib  
```
  
Experimental data are kept in CSV files because CSV files can be edited in any  
text editor without Microsoft Excel. But you have to be careful not to delete any comma.   
Its also easy to import data via CSV file to OriginLab by going to   
File >> Import >> CSV file

Python has built-in csv module to read and write CSV file go.
  
---
  

# Steps

### __#1__ Copy Project files
Right click on empty space on desktop. Then click "Git Bash Here" .   
  
![Step: 1.1](./images/1.png)
  
A terminal will pop  up.  
Then ENTER this command below:  
  
```
git clone https://bitbucket.org/foy4748/a12_automate
```
  
![Step: 1.2](./images/2.png)
  
A folder name `a12_automate` will be copyied to desktop. Double Click to enter into the folder. 
  
![Step: 1.3](./images/2_2.png)
### __#2__ Editing Data  
Now open `A12_R0.csv` and `A12_Data.csv` files using Microsoft Excel.  
  
Input experimental data according to the units shown at the column labels. 
  
![Step: 2](./images/3.png)
  
### __#3__ Magic !!
Double click `process` or `process.py` . A terminal will pop up. Enter the room temperature to proceed.  
  
![Step: 3.1](./images/4.png)
  
The zero temperature Resistance, Calculated Data, Slope of  
the linear fitted curve will be printed out on the terminal.  
Then _Figure 1_ window will pop up displaying the Log-Log plot !!!.  
  
![Step: 3.2](./images/5.png)
  
Close the plot window.  
Checkout `results.csv` using Microsoft Excel. The necessary data to plot the curve is  
written to this CSV file. You may copy this data to OriginLab to plot the rest.

